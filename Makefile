.PHONY: all

ZIP = TreballFinalMaster-defensa.zip
ORIGIN_PATH = ~/Downloads/${ZIP}
LATEX = \
	main.tex \
	*/	\

all:
	realpath ${ORIGIN_PATH}
	rm -rf ${LATEX}
	mv ${ORIGIN_PATH} .
	unzip -o ${ZIP}
	rm ${ZIP}
