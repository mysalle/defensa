#!/bin/bash -xe
session="Def"

if ! tmux new-session -d -s $session ; then
	read -p "You want to continue?"
fi

window=0
tmux rename-window -t $session:$window 'moodle'
tmux send-keys -t $session:$window 'cd ~/Documents/tfm/moodle' C-m
tmux send-keys -t $session:$window 'docker-compose up' C-m

window=1
tmux new-window -t $session:$window -n 'es'
tmux send-keys -t $session:$window 'cd ~/Documents/tfm/logstash/elasticsearch' C-m
tmux send-keys -t $session:$window 'make' C-m

window=2
tmux new-window -t $session:$window -n 'help'
tmux send-keys -t $session:$window 'echo http://127.0.0.1:8000/moodle/' C-m
tmux send-keys -t $session:$window 'echo http://localhost:5601/' C-m

window=3
tmux new-window -t $session:$window -n 'ls'
tmux send-keys -t $session:$window 'cd ~/Documents/tfm/logstash' C-m
